package btswalletgo

import (
	"encoding/json"
	"fmt"
)

type (
	ReponseWitness struct {
		ID     int64         `json:"id"`
		Result ResultWitness `json:"result"`
	}

	ResultWitness struct {
		ID                    string `json:"id"`
		WitnessAccount        string `json:"witness_account"`
		LastAslot             uint64 `json:"last_aslot"`
		SigningKey            string `json:"signing_key"`
		PayVB                 string `json:"pay_vb"`
		VoteID                string `json:"vote_id"`
		TotalVotes            string `json:"total_votes"`
		URL                   string `json:"url"`
		TotalMissed           uint64 `json:"total_missed"`
		LastConfirmedBlockNum uint64 `json:"last_confirmed_block_num"`
	}
)

func (b *BTS) UpdateWitness(witness, url, key string, broadcast bool) {
	post := Request{
		JSONRPC: "2.0",
		Method:  "update_witness",
		Params:  []interface{}{witness, url, key, broadcast},
		Id:      1,
	}
	b.post(post)
}

func (b *BTS) GetWitness(witness string) ReponseWitness {
	post := Request{
		JSONRPC: "2.0",
		Method:  "get_witness",
		Params:  []interface{}{witness},
		Id:      1,
	}
	resp := ReponseWitness{}
	err := json.Unmarshal(b.post(post), &resp)
	if err != nil {
		fmt.Println("getWitness error: json unmarshal")
		panic(err)
	}
	return resp
}
