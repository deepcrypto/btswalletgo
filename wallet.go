package btswalletgo

import (
	"encoding/json"
	"fmt"
)

func (b *BTS) IsNew() bool {
	post := Request{
		JSONRPC: "2.0",
		Method:  "is_new",
		Params:  []interface{}{},
		Id:      1,
	}
	resp := ResponseBool{}
	err := json.Unmarshal(b.post(post), &resp)
	if err != nil {
		fmt.Println("isNew error: json unmarshal")
		panic(err)
	}

	return resp.Result
}

func (b *BTS) IsLocked() bool {
	post := Request{
		JSONRPC: "2.0",
		Method:  "is_locked",
		Params:  []interface{}{},
		Id:      1,
	}
	resp := ResponseBool{}
	err := json.Unmarshal(b.post(post), &resp)
	if err != nil {
		fmt.Println("isLocked error: json unmarshal")
		panic(err)
	}
	return resp.Result
}

func (b *BTS) Unlock() {
	post := Request{
		JSONRPC: "2.0",
		Method:  "unlock",
		Params:  []interface{}{b.Password},
		Id:      1,
	}
	_ = b.post(post)
}

func (b *BTS) Lock() {
	post := Request{
		JSONRPC: "2.0",
		Method:  "lock",
		Params:  []interface{}{},
		Id:      1,
	}
	b.post(post)
}
