package btswalletgo

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type (
	BTS struct {
		Node        string
		Client      *http.Client
		Password    string
		ContentType string
	}

	Request struct {
		JSONRPC string        `json:"jsonrpc"`
		Method  string        `json:"method"`
		Params  []interface{} `json:"params"`
		Id      int           `json:"id"`
	}

	ResponseBool struct {
		Id     int  `json:"id"`
		Result bool `json:"result"`
	}
)

func (b *BTS) post(post Request) []byte {
	jsonRequest, err := json.Marshal(post)
	if err != nil {
		fmt.Println("Post Error: json marshalling")
		panic(err)
	}

	res, err := b.Client.Post(b.Node, b.ContentType, bytes.NewBuffer(jsonRequest))
	if err != nil {
		fmt.Println("Post Error: http post")
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("Post error: http reponse")
		panic(err)
	}
	return body
}

func (b *BTS) Connect() {
	b.ContentType = "application/json"
	tr := &http.Transport{
		DisableKeepAlives: true,
	}
	b.Client = &http.Client{Transport: tr}
}
